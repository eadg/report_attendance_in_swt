package com.mooo.rokicki.dziennik;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class CheckKids {

	public CheckKids(String s) {
		kidsSortReady = new TreeMap<Integer, Object[]>();
		comment = new StringBuilder();
		multiChilds = new Boolean("False");
		getMaps(s);
		iterPrev();
		iterNext();
	}

	private TreeMap<Integer, Object[]> kidsSortPrev;
	private TreeMap<Integer, Object[]> kidsSortNext;
	private TreeMap<Integer, Object[]> kidsSortReady;
	private StringBuilder comment;
	private Boolean	multiChilds; 

	public TreeMap<Integer, Object[]> getKidsSortReady() {
		return kidsSortReady;
	}

	private void getMaps(String s) {
		FromLO flo = new FromLO(s);
		List<TreeMap<Integer, Object[]>> listaMap = flo.getMapKidss();
		this.kidsSortPrev = (TreeMap<Integer, Object[]>) listaMap.get(0);
		this.kidsSortNext = (TreeMap<Integer, Object[]>) listaMap.get(1);
	}

	// private void kontynuacja() {
	// comment.append( "kontynuacja");
	// }

	private void pakiet(Integer i) {
		if (kidsSortNext.get(i)[2] != kidsSortPrev.get(i)[2]) {
			comment.append("zmiana_pakietu");
		}
	}

	private void mops(Integer i) {
		if ((Integer) kidsSortNext.get(i)[8] == 0) {
			comment.append(" mops");
		}
	}

	private void procent(Integer i) {
		Double dprev = (Double) kidsSortPrev.get(i)[6];
		Double dnext = (Double) kidsSortNext.get(i)[6];
		if ((dprev - dnext >= 0.2) || (dprev - dnext <= -0.2)) {
				comment.append(" zmiana_%_opłaty");
		}
	}

	private void wypis() {
		comment.append("wypis");
	}

	private void newKids() {
		comment.append("nowe dziecko");
	}
	
	private void checkMultiChilds(Integer i) {
		Double dnext = (Double) kidsSortNext.get(i)[6];
		if (dnext < 0.1) {
			multiChilds = true;
		}
	}
	
	private void clearComments() {
		multiChilds = false;
		comment.setLength(0);
	}

	private void iterPrev() {
		// Przeszukuje klucze z poprzedniego miesiąca i porównuje z bieżącym,
		// co daje info o wypisanych.
		// Jeśli klucze występują w obu miesiącach sprawdza pakiety,
		// czy się zmieniły.
		Integer keyp;
		Iterator<Entry<Integer, Object[]>> iter = kidsSortPrev.entrySet()
				.iterator();
		while (iter.hasNext()) {
			Map.Entry<Integer, Object[]> mapEntry = (Map.Entry<Integer, Object[]>) iter
					.next();
			keyp = mapEntry.getKey();

			if (kidsSortNext.containsKey(keyp)) {
				// kontynuacja();
				pakiet(keyp);
				mops(keyp);
				procent(keyp);
				checkMultiChilds(keyp);

				kidsSortReady.put(keyp, new Object[] {
						kidsSortPrev.get(keyp)[0], kidsSortPrev.get(keyp)[1],
						kidsSortNext.get(keyp)[2], kidsSortPrev.get(keyp)[3],
						kidsSortPrev.get(keyp)[4], kidsSortPrev.get(keyp)[5],
						kidsSortNext.get(keyp)[6], comment.toString(), multiChilds });
				clearComments();

			} else {

				wypis();
				kidsSortReady.put(keyp, new Object[] {
						kidsSortPrev.get(keyp)[0], kidsSortPrev.get(keyp)[1],
						kidsSortPrev.get(keyp)[2], kidsSortPrev.get(keyp)[3],
						kidsSortPrev.get(keyp)[4], kidsSortPrev.get(keyp)[5],
						kidsSortPrev.get(keyp)[6], comment.toString(), multiChilds });
				clearComments();
			}
		}
	}

	private void iterNext() {
		// Przeszukuje poprzedni miesiąc po kluczach z bieżącego.
		// Jeżeli znajdzie to update uwagi "nowe dziecko"
		Integer keyn;
		Iterator<Entry<Integer, Object[]>> iter = kidsSortNext.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<Integer, Object[]> mapEntry = (Map.Entry<Integer, Object[]>) iter.next();
			keyn = mapEntry.getKey();
			if (!kidsSortPrev.containsKey(keyn)) {
				newKids();
				mops(keyn);
				checkMultiChilds(keyn);
				kidsSortReady.put(keyn,	new Object[] { 
						kidsSortNext.get(keyn)[0], 	kidsSortNext.get(keyn)[1],
						kidsSortNext.get(keyn)[2],	kidsSortNext.get(keyn)[3],
						kidsSortNext.get(keyn)[4],	new Integer(0),
						kidsSortNext.get(keyn)[6],	comment.toString(), multiChilds });
				clearComments();
			}

		}
	}

}
