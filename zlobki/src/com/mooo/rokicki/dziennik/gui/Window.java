package com.mooo.rokicki.dziennik.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import com.mooo.rokicki.dziennik.BazaExcel;
import com.mooo.rokicki.dziennik.DateIP;
import com.mooo.rokicki.dziennik.Version;

import org.eclipse.wb.swt.SWTResourceManager;

public class Window {

	public Window() {
		dip = new DateIP();
	}

	protected Shell shlGenerujBaz;
	private DateIP dip;
	protected Button backupButton;
	protected Boolean backup;  
	
	public Boolean getBackup() {
		return backup;
	}

	public void setBackup(Boolean backup) {
		this.backup = backup;
	}

	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlGenerujBaz.open();
		shlGenerujBaz.layout();
		while (!shlGenerujBaz.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	protected void createContents() {
		shlGenerujBaz = new Shell();
		shlGenerujBaz.setSize(270, 246);
		shlGenerujBaz.setText("Generuj Bazę");
		shlGenerujBaz.setLayout(new GridLayout(1, false));
		

		Label labelWybor = new Label(shlGenerujBaz, SWT.NONE);
		labelWybor.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.NORMAL));
		// lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 14,
		// SWT.BOLD));
		GridData gd_lblNewLabel = new GridData(SWT.CENTER, SWT.BOTTOM, true,
				false, 1, 1);
		gd_lblNewLabel.heightHint = 19;
		labelWybor.setLayoutData(gd_lblNewLabel);
		labelWybor.setText("Wybór miesiąca:");
		
				final Label labelMiesiace = new Label(shlGenerujBaz, SWT.CENTER);
				labelMiesiace.setText("Baza za okres: ".concat(dip
						.getPrevNazwaMiesiac().concat(" - ")
						.concat(dip.getNazwaMiesiac())));
				labelMiesiace.setAlignment(SWT.CENTER);
				GridData gd_labelMiesiace = new GridData(SWT.CENTER, SWT.CENTER, false,
						false, 1, 1);
				gd_labelMiesiace.widthHint = 238;
				labelMiesiace.setLayoutData(gd_labelMiesiace);
				
						final Combo combo = new Combo(shlGenerujBaz, SWT.READ_ONLY);
						combo.setItems(dip.getArrayMonthString());
						combo.select(dip.getArrayMonthString().length - 1);
						combo.addSelectionListener(new SelectionAdapter() {
							@Override
							public void widgetSelected(SelectionEvent arg0) {
								labelMiesiace.setText("Baza za okres: ".concat(dip
										.getPrevMonth(combo.getText()).concat(" - ")
										.concat(combo.getText())));
							}
						});
						
								GridData gd_combo = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
								gd_combo.widthHint = 239;
								combo.setLayoutData(gd_combo);
								new Label(shlGenerujBaz, SWT.NONE);
						
								backupButton = new Button(shlGenerujBaz, SWT.CHECK);
								backupButton.setSelection(true);
								backupButton.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
								backupButton.setText("Wyślij kopię dzienników na serwer");
						new Label(shlGenerujBaz, SWT.NONE);
				
						Button buttonOK = new Button(shlGenerujBaz, SWT.NONE);
						GridData gridButtonOK = new GridData(SWT.CENTER, SWT.CENTER, false,	false, 1, 1);
						gridButtonOK.widthHint = 220;
						buttonOK.setLayoutData(gridButtonOK);
						buttonOK.setText("OK");
						buttonOK.addSelectionListener(new SelectionAdapter() {
							public void widgetSelected(SelectionEvent arg0) {
								BazaExcel.setMonth(combo.getText());
								setBackup(backupButton.getSelection());
								shlGenerujBaz.close();
							}
						});
				new Label(shlGenerujBaz, SWT.NONE);
				
				Label labelWersja = new Label(shlGenerujBaz, SWT.NONE);
				labelWersja.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
				labelWersja.setText(Version.getV());
	}

}