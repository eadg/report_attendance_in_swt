package com.mooo.rokicki.dziennik.test;

import com.mooo.rokicki.dziennik.DateIP;
import com.mooo.rokicki.dziennik.backup.*;

public class TestTarFtp {
	
	public static void main(String[] args) {
		Tarka t = new Tarka();
		t.tar(new DateIP().getPathFiles());
		String r = t.getTarName();
		String l = t.getTarPath();
		Uftp uftp = new Uftp();
		String rez = uftp.upload(l, r);
		System.out.println(rez);
	}
}
