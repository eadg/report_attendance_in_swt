package com.mooo.rokicki.dziennik;

import java.text.NumberFormat;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ToExcel {

	void loadToExcel(TreeMap<Integer, Object[]> tm) {

		int i = 0;
		NumberFormat percentFormat = NumberFormat.getPercentInstance();
		percentFormat.setMaximumFractionDigits(0);
		ExcelTemplate et = new ExcelTemplate();
		Workbook wb = et.setTemplate();
		Object[] kolumny;
		Sheet sheet = wb.getSheetAt(0);
		Row row;

		Iterator<Entry<Integer, Object[]>> iterator = tm.entrySet().iterator();
		while (iterator.hasNext()) {
			i++;
			Map.Entry<Integer, Object[]> mapEntry = (Map.Entry<Integer, Object[]>) iterator.next();
			kolumny = mapEntry.getValue();
			row = sheet.createRow((short) i);
			Cell[] celtab = et.setCellTab();

			celtab[0] = row.createCell(0);
			celtab[0].setCellValue(mapEntry.getKey());
			celtab[0].setCellStyle(et.setCenter());

			celtab[1] = row.createCell(1);
			celtab[1].setCellValue((String) kolumny[0]);
			celtab[1].setCellStyle(et.setCenter());

			celtab[2] = row.createCell(2);
			celtab[2].setCellValue((String) kolumny[1]);
			celtab[2].setCellStyle(et.setCenter());

			celtab[3] = row.createCell(3);
			celtab[3].setCellValue((Integer) kolumny[2]);
			celtab[3].setCellStyle(et.setCenter());

			celtab[4] = row.createCell(4);
			celtab[4].setCellValue((Integer) kolumny[3]);
			celtab[4].setCellStyle(et.setCenter());

			celtab[5] = row.createCell(5);
			celtab[5].setCellValue((Integer) kolumny[4]);
			celtab[5].setCellStyle(et.setCenter());

			celtab[6] = row.createCell(6);
			celtab[6].setCellValue((Integer) kolumny[5]);
			celtab[6].setCellStyle(et.setCenter());

			celtab[7] = row.createCell(7);
			// Double d = ((Double)kolumny[7]);
			celtab[7].setCellValue(percentFormat.format(((Double) kolumny[6]).doubleValue()));
			celtab[7].setCellStyle(et.setCenter());

			celtab[8] = row.createCell(8);
			celtab[8].setCellValue((String) kolumny[7]);
			celtab[8].setCellStyle(et.setCenter());
			
			celtab[9] = row.createCell(9);
			if((Boolean)kolumny[8]) {
				celtab[9].setCellValue("tak");
			}
			else {
				celtab[9].setCellValue("nie");
			}
			
			celtab[9].setCellStyle(et.setCenter());
		}
		et.setAutoCenter(wb);
		et.saveTemplate(wb);

	}
}
