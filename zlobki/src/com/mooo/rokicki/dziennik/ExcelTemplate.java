package com.mooo.rokicki.dziennik;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.jopendocument.dom.OOUtils;

//import com.mooo.rokicki.dziennik.backup.Mailing;

public class ExcelTemplate {

	private String[] naglowki_kolumn = { "Kontrahent", "Nazwisko", "Imię",
			"Pakiet", "Spóźn do 10h", "Spóźn ponad 10h", "Odpisy",
			"Procent opłaty", "Uwagi", "Wielodzietność" };

	private Workbook wb;
	private DateIP dip;
//	private Mailing mail;

	public ExcelTemplate() {
		wb = new HSSFWorkbook();
		dip = new DateIP();
	}

	public int getTemplateLength() {
		return naglowki_kolumn.length;
	}

	public CellStyle setCenter() {
		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		return cellStyle;
	}

	public void setAutoCenter(Workbook work) {
		Sheet sheet = work.getSheetAt(0);
		for (int i = 0; i < naglowki_kolumn.length; i++) {
			sheet.autoSizeColumn(i);
		}
	}

	public Workbook setTemplate() {
		Sheet sheet = wb.createSheet(dip.getPrevMonth(BazaExcel.getMonth())
				+ "-" + BazaExcel.getMonth());
		Row row = sheet.createRow(0);

		Cell[] celtab = setCellTab();

		for (int i = 0; i < naglowki_kolumn.length; i++) {
			celtab[i] = row.createCell(i);
			celtab[i].setCellStyle(setCenter());
			celtab[i].setCellValue(naglowki_kolumn[i]);
		}
		return wb;
	}

	public Cell[] setCellTab() {
		return new Cell[naglowki_kolumn.length];
	}

	public void saveTemplate(Workbook work) {
		try {
			File file = new File(
					"c:/Dziennik_Elektroniczny/baza_export_z_dziennika/baza_zlobek_"
							+ dip.getNumberNurse() + "_" + dip.getRok() + "_"
							+ dip.getPrevMonth(BazaExcel.getMonth()) + "-"
							+ BazaExcel.getMonth() + ".xls");
			FileOutputStream fileOut = new FileOutputStream(file);
			work.write(fileOut);
			fileOut.close();
			OOUtils.open(file);
		} catch (IOException ioe) {
			System.out.println(ioe);
		}
	}

}
