package com.mooo.rokicki.dziennik;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeMap;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class FromLO {

	private TreeMap<Integer, Object[]> kidsSortPrev;
	private TreeMap<Integer, Object[]> kidsSortNext;
	private DateIP dip;
	private File input;
	private String month;

	public FromLO(String month) {
		this.month = month;
		kidsSortPrev = new TreeMap<Integer, Object[]>();
		kidsSortNext = new TreeMap<Integer, Object[]>();
		dip = new DateIP();
	}

	private void grabFromLLO() {

		Object rawCell;
		Object[] kidArrayPrev;
		Object[] kidArrayNext;
		Integer kontrahentPrev;
		Integer kontrahentNext;

		for (String nazwapliku : dip.getPathFiles()) {
			int i = 0;
			int j = 0;
			input = new File(nazwapliku);

			org.jopendocument.dom.spreadsheet.Sheet sheet_prev = null;
			org.jopendocument.dom.spreadsheet.Sheet sheet_next = null;
			try {
				sheet_prev = SpreadSheet.createFromFile(input).getSheet(
						dip.getPrevMonth(month));
				sheet_next = SpreadSheet.createFromFile(input).getSheet(month);

				// sheet_prev = SpreadSheet.createFromFile(input).getSheet(
				// dip.getPrevNazwaMiesiac());
				// sheet_next = SpreadSheet.createFromFile(input).getSheet(
				// dip.getNazwaMiesiac());
				// sheet_prev = SpreadSheet.createFromFile(input).getSheet(
				// dip.getPrevNrArkusza());
				// sheet_next = SpreadSheet.createFromFile(input).getSheet(
				// dip.getNrArkusza());

			} catch (IOException ioe) {
				System.out.println(ioe);
			}
			do {
				i++;
				kidArrayPrev = new Object[9];
				rawCell = sheet_prev.getValueAt(2, i);
				if (rawCell.getClass().getSimpleName().equals("BigDecimal")
						&& ((BigDecimal) rawCell).intValue() > 0) {
					kontrahentPrev = ((BigDecimal) rawCell).intValue();
				} else {
					continue;
				}
				kidArrayPrev[0] = sheet_prev.getValueAt(3, i).toString();
				kidArrayPrev[1] = sheet_prev.getValueAt(4, i).toString();
				try {
					kidArrayPrev[2] = ((BigDecimal) sheet_prev
							.getValueAt(46, i)).intValue();
				} catch (ClassCastException cce) {
					kidArrayPrev[2] = 0;
				}
				try {
					kidArrayPrev[3] = ((BigDecimal) sheet_prev
							.getValueAt(47, i)).intValue();
				} catch (ClassCastException cce) {
					kidArrayPrev[3] = 0;
				}
				try {
					kidArrayPrev[4] = ((BigDecimal) sheet_prev
							.getValueAt(48, i)).intValue();
				} catch (ClassCastException cce) {
					kidArrayPrev[4] = 0;
				}
				try {
					kidArrayPrev[5] = ((BigDecimal) sheet_prev
							.getValueAt(43, i)).intValue();
				} catch (ClassCastException cce) {
					kidArrayPrev[5] = 0;
				}
				try {
					kidArrayPrev[6] = ((BigDecimal) sheet_prev
							.getValueAt(57, i)).doubleValue();
				} catch (ClassCastException cce) {
					kidArrayPrev[6] = 0.00;
				}
				kidArrayPrev[7] = "";
				kidsSortPrev.put(kontrahentPrev, kidArrayPrev);
			} while (i < 65);

			do {
				j++;
				kidArrayNext = new Object[9];
				rawCell = sheet_next.getValueAt(2, j);
				if (rawCell.getClass().getSimpleName().equals("BigDecimal")
						&& ((BigDecimal) rawCell).intValue() > 0) {
					kontrahentNext = ((BigDecimal) rawCell).intValue();
				} else {
					continue;
				}
				kidArrayNext[0] = sheet_next.getValueAt(3, j).toString();
				kidArrayNext[1] = sheet_next.getValueAt(4, j).toString();
				try {
					kidArrayNext[2] = ((BigDecimal) sheet_next
							.getValueAt(46, j)).intValue();
				} catch (ClassCastException cce) {
					kidArrayNext[2] = 0;
				}
				try {
					kidArrayNext[3] = ((BigDecimal) sheet_next
							.getValueAt(47, j)).intValue();
				} catch (ClassCastException cce) {
					kidArrayNext[3] = 0;
				}
				try {
					kidArrayNext[4] = ((BigDecimal) sheet_next
							.getValueAt(48, j)).intValue();
				} catch (ClassCastException cce) {
					kidArrayNext[4] = 0;
				}
				try {
					kidArrayNext[5] = ((BigDecimal) sheet_next
							.getValueAt(43, j)).intValue();
				} catch (ClassCastException cce) {
					kidArrayNext[5] = 0;
				}
				try {
					kidArrayNext[6] = ((BigDecimal) sheet_next
							.getValueAt(57, j)).doubleValue();
				} catch (ClassCastException cce) {
					kidArrayNext[6] = 0.00;
				}
				kidArrayNext[7] = "";

				try {
					kidArrayNext[8] = ((BigDecimal) sheet_next
							.getValueAt(61, j)).intValue();
				} catch (ClassCastException cce) {
					kidArrayNext[8] = 1;
				}

				kidsSortNext.put(kontrahentNext, kidArrayNext);
			} while (j < 65);

			sheet_prev.detach();
			sheet_next.detach();
		}
	}

	List<TreeMap<Integer, Object[]>> getMapKidss() {
		grabFromLLO();
		List<TreeMap<Integer, Object[]>> lista = new ArrayList<TreeMap<Integer, Object[]>>();
		lista.add(kidsSortPrev);
		lista.add(kidsSortNext);
		return lista;
	}
}
