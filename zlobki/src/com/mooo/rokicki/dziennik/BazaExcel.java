package com.mooo.rokicki.dziennik;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import com.mooo.rokicki.dziennik.backup.Tarka;
import com.mooo.rokicki.dziennik.backup.Uftp;
import com.mooo.rokicki.dziennik.gui.Window;

public class BazaExcel extends Thread{
	
	public BazaExcel() {
		super();
		this.tarka = new Tarka();
		this.win = new Window();
		win.open();
	}

	private Window win;
	private Tarka tarka;
	private static String month;

	public static String getMonth() {
		return month;
	}

	public static void setMonth(String month) {
		BazaExcel.month = month;
	}
	
	public void taruj() {
		tarka.tar(new DateIP().getPathFiles());
	}
	
	public void run() {
			String r = tarka.getTarName();
			String l = tarka.getTarPath();
			Uftp uftp = new Uftp();
			uftp.upload(l, r);
	}

	public static void main(String[] args) {

		BazaExcel be = new BazaExcel();
		if (BazaExcel.getMonth() != null) {
			try {
				System.setOut(new PrintStream(new FileOutputStream(
						java.io.FileDescriptor.out), true, "Cp852"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			
			if(be.win.getBackup()){
				be.taruj();
				be.start();
			}
			
			System.out.println("Proszę czekać trwa przetwarzanie danych z dzienników");
			ProbitCheck cp = new ProbitCheck(BazaExcel.getMonth());
			cp.toCSV();
//			CheckKids foo = new CheckKids(BazaExcel.getMonth());
//			ToExcel te = new ToExcel();
//			te.loadToExcel(foo.getKidsSortReady());
						
		}

	}
}
