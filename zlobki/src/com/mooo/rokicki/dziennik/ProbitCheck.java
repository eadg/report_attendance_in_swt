package com.mooo.rokicki.dziennik;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.csvreader.CsvWriter;

public class ProbitCheck {

	public ProbitCheck(String s) {
		try {
			csvOutput = new CsvWriter(new FileWriter("/tmp/out.csv", false), ';');
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		getMaps(s);
	}

	private CsvWriter csvOutput;
	
	private TreeMap<Integer, Object[]> kidsSortPrev;
//	private TreeMap<Integer, Object[]> kidsSortNext;

	private void getMaps(String s) {
		ProbitFromLO pflo = new ProbitFromLO(s);
		List<TreeMap<Integer, Object[]>> listaMap = pflo.getMapKidss();
		this.kidsSortPrev = (TreeMap<Integer, Object[]>) listaMap.get(0);
//		this.kidsSortNext = (TreeMap<Integer, Object[]>) listaMap.get(1);
	}

	private String makeString5(int i) {
		String ss = null;
		String s = new Integer(i).toString();
		if (s.length() < 5) {
			ss = '0'+s;
		}
		else {
			ss = s;
		}
		return ss;
	}	
	
	private String jakiPakiet100(int i, double ii) {
		String s = null;
		Double double_in = new Double(ii);
		Double double_out = new BigDecimal(double_in).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
		switch(i) {
		case 8:  	s="00004|"+double_out;	break;
		case 9:  	s="00005|"+double_out;	break;
		case 10: 	s="00006|"+double_out;	break;
		}
		return s;
	}
	
	private String jakiPakiet48(int i, double ii) {
		String s = null;
		Double double_in = new Double(ii);
		Double double_out = new BigDecimal(double_in).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
		switch(i) {
		case 8:  	s="00009|"+double_out;	break;
		case 9:  	s="00010|"+double_out;	break;
		case 10: 	s="00011|"+double_out;	break;
		}
		return s;
	}

	private String jakiPakiet0(int i, double ii) {
		String s = null;
		Double double_in = new Double(ii);
		Double double_out = new BigDecimal(double_in).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
		switch(i) {
		case 8:  	s="00012|"+double_out;	break;
		case 9:  	s="00012|"+double_out;	break;
		case 10: 	s="00012|"+double_out;	break;
		}
		return s;
	}	
	
	private String przekroczenie8h(int i) {
		String s = null;
		double ile = i * 1.45;
		s="00007|"+ile;
		return s;
	}
	
	private String przekroczenie10h(int i) {
		String s = null;
		double ile = i * 20;
		s="00008|"+ile;
		return s;
	}	

	public void toCSV() {
		try {
			
			Integer keyp;
			Iterator<Entry<Integer, Object[]>> iter = kidsSortPrev.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry<Integer, Object[]> mapEntry = (Map.Entry<Integer, Object[]>) iter.next();
				keyp = mapEntry.getKey();
				csvOutput.write("00001|" + makeString5(keyp));
				csvOutput.write("00002|" + kidsSortPrev.get(keyp)[0]);
				csvOutput.write("00003|" + kidsSortPrev.get(keyp)[1]);
				if((double)kidsSortPrev.get(keyp)[6] == 1.00) {
					csvOutput.write(jakiPakiet100((int) kidsSortPrev.get(keyp)[2], (double) kidsSortPrev.get(keyp)[3]));
				}
				csvOutput.write(przekroczenie8h((int) kidsSortPrev.get(keyp)[4]));
				csvOutput.write(przekroczenie10h((int) kidsSortPrev.get(keyp)[5]));
				if((double)kidsSortPrev.get(keyp)[6] < 0.50 && (double)kidsSortPrev.get(keyp)[6] > 0.40) {
					csvOutput.write(jakiPakiet48((int) kidsSortPrev.get(keyp)[2], (double) kidsSortPrev.get(keyp)[3]));
				}
				if((double)kidsSortPrev.get(keyp)[6] == 0) {
					csvOutput.write(jakiPakiet0((int) kidsSortPrev.get(keyp)[2], (double) kidsSortPrev.get(keyp)[3]));
				}
				csvOutput.endRecord();
	
//				csvOutput.write("");
//				csvOutput.write("");
			}
			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
