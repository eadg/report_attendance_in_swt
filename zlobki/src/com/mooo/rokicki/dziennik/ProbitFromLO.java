package com.mooo.rokicki.dziennik;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeMap;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class ProbitFromLO {

	private TreeMap<Integer, Object[]> kidsSortPrev;
	private TreeMap<Integer, Object[]> kidsSortNext;
	private DateIP dip;
	private File input;
	private String month;

	public ProbitFromLO(String month) {
		this.month = month;
		kidsSortPrev = new TreeMap<Integer, Object[]>();
		kidsSortNext = new TreeMap<Integer, Object[]>();
		dip = new DateIP();
	}
	
	List<TreeMap<Integer, Object[]>> getMapKidss() {
		grabFromLLO();
		List<TreeMap<Integer, Object[]>> lista = new ArrayList<TreeMap<Integer, Object[]>>();
		lista.add(kidsSortPrev);
		lista.add(kidsSortNext);
		return lista;
	}	

	private void grabFromLLO() {

		Object rawCell;
		Object[] kidArrayPrev;
//		Object[] kidArrayNext;
		Integer kontrahentPrev;
//		Integer kontrahentNext;

		for (String nazwapliku : dip.getPathFiles()) {
			int i = 0;
//			int j = 0;
			input = new File(nazwapliku);

			org.jopendocument.dom.spreadsheet.Sheet sheet_prev = null;
			org.jopendocument.dom.spreadsheet.Sheet sheet_next = null;
			try {
				sheet_prev = SpreadSheet.createFromFile(input).getSheet(
						dip.getPrevMonth(month));
				sheet_next = SpreadSheet.createFromFile(input).getSheet(month);

			} catch (IOException ioe) {
				System.out.println(ioe);
			}
			do {
				i++;
				kidArrayPrev = new Object[9];
				rawCell = sheet_next.getValueAt(2, i);
				if (rawCell.getClass().getSimpleName().equals("BigDecimal")
						&& ((BigDecimal) rawCell).intValue() > 0) {
					kontrahentPrev = ((BigDecimal) rawCell).intValue();
				} else {
					continue;
				}
				try {
					kidArrayPrev[0] = ((BigDecimal) sheet_next
							.getValueAt(51, i)).doubleValue();
				} catch (ClassCastException cce) {
					kidArrayPrev[0] = 0;
				}				
				try {
					kidArrayPrev[1] = ((BigDecimal) sheet_next
							.getValueAt(53, i)).doubleValue();
				} catch (ClassCastException cce) {
					kidArrayPrev[1] = 0.00;
				}
				try {
					kidArrayPrev[2] = ((BigDecimal) sheet_next
							.getValueAt(46, i)).intValue();
				} catch (ClassCastException cce) {
					kidArrayPrev[2] = 0;
				}
				try {
					kidArrayPrev[3] = ((BigDecimal) sheet_next
							.getValueAt(50, i)).doubleValue();
				} catch (ClassCastException cce) {
					kidArrayPrev[3] = 0.00;
				}
				try {
					kidArrayPrev[4] = ((BigDecimal) sheet_prev
							.getValueAt(47, i)).intValue();
				} catch (ClassCastException cce) {
					kidArrayPrev[4] = 0;
				}
				try {
					kidArrayPrev[5] = ((BigDecimal) sheet_prev
							.getValueAt(48, i)).intValue();
				} catch (ClassCastException cce) {
					kidArrayPrev[5] = 0;
				}				
				try {
					kidArrayPrev[6] = ((BigDecimal) sheet_next
							.getValueAt(57, i)).doubleValue();
				} catch (ClassCastException cce) {
					kidArrayPrev[6] = 0.00;
				}
				kidsSortPrev.put(kontrahentPrev, kidArrayPrev);
			} while (i < 65);

			sheet_prev.detach();
			sheet_next.detach();
		}
	}


}
