package com.mooo.rokicki.dziennik;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.util.List;

public class DateIP {

	private final String[][] nazwyPlikow = {
			{ "zl_9_gr_1_", "zl_9_gr_2_" },
			{ "zl_1_gr_1_", "zl_1_gr_2_", "zl_1_gr_3_", "zl_1_gr_4_" },
			{ "zl_2_gr_1_", "zl_2_gr_2_", "zl_2_gr_3_" },
			{ "zl_3_gr_1_", "zl_3_gr_2_", "zl_3_gr_3_" },
			{ "zl_4_gr_1_", "zl_4_gr_2_", "zl_4_gr_3_" },
			{ "zl_5_gr_1_", "zl_5_gr_2_", "zl_5_gr_3_" },
			{ "zl_6_gr_1_", "zl_6_gr_2_", "zl_6_gr_3_" },
			{ "zl_7_gr_1_", "zl_7_gr_2_", "zl_7_gr_3_" },
			{ "zl_8_gr_1_", "zl_8_gr_2_", "zl_8_gr_3_", "zl_8_gr_4_" },
			{ "zl_9_gr_1_", "zl_9_gr_2_" },
			{ "zl_10_gr_1_", "zl_10_gr_2_", "zl_10_gr_3_", "zl_10_gr_4_" },
			{ "zl_11_gr_1_", "zl_11_gr_2_", "zl_11_gr_3_" },
			{ "zl_12_gr_1_", "zl_12_gr_2_", "zl_12_gr_3_" },
			{ "zl_13_gr_1_", "zl_13_gr_2_", "zl_13_gr_3_" },
			{ "zl_14_gr_1A_", "zl_14_gr_1B_", "zl_14_gr_2A_", "zl_14_gr_2B_",
					"zl_14_gr_3A_", "zl_14_gr_3B_", "zl_14_gr_3C_" },
			{ "zl_15_gr_1A_", "zl_15_gr_2A_", "zl_15_gr_2B_", "zl_15_gr_2C_", 
					"zl_15_gr_3A_", "zl_15_gr_3B_", "zl_15_gr_3C_",
					"zl_15_gr_3D_" }, };

	private final String[] nameSheet = { "Sty", "Lut", "Mar", "Kwi", "Maj",
			"Cze", "Lip", "Sie", "Wrz", "Paz", "Lis", "Gru" };

	private final String[] nameSheetMonth = { "Wrz", "Paz", "Lis", "Gru",
			"Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie" };

	private final int[] nrArkusza = { 10, 12, 14, 16, 18, 20, 22, 24, 2, 4, 6,
			8 };

	private Calendar cal = Calendar.getInstance();

	public int getMiesiac() {
		cal.setTime(new Date());
		return (cal.get(Calendar.MONTH) + 1);
	}

	public int getRok() {
		cal.setTime(new Date());
		return (cal.get(Calendar.YEAR));
	}
	
	public String getData() {
		cal.setTime(new Date());
		String s = new Integer(cal.get(Calendar.YEAR)).toString() + (cal.get(Calendar.MONTH)+1) + cal.get(Calendar.DAY_OF_MONTH) + cal.get(Calendar.HOUR_OF_DAY) + cal.get(Calendar.MINUTE); 
		return s;
	}

	public String[] getArrayMonthString() {
		List<String> als = new ArrayList<String>();
		for (String str : nameSheetMonth) {
			als.add(str);
			if (str.equalsIgnoreCase(getNazwaMiesiac()))
				break;
		}
		return als.toArray(new String[als.size()]);
	}

	public String getPrevMonth(String s) {
		String sout = "";
		for (int index = 0; index < nameSheetMonth.length; index++) {
			if (nameSheetMonth[index].equalsIgnoreCase(s)) {
				if (index > 0) {
					sout = nameSheetMonth[index - 1];
				} else {
					sout = nameSheetMonth[nameSheetMonth.length - 1];
				}
			}
		}
		return sout;
	}

	public String getNazwaMiesiac() {
		cal.setTime(new Date());
		return nameSheet[(cal.get(Calendar.MONTH))];
	}

	public String getPrevNazwaMiesiac() {
		cal.setTime(new Date());
		if (cal.get(Calendar.MONTH) > 0) {
			return nameSheet[(cal.get(Calendar.MONTH)) - 1];
		} else {
			return nameSheet[11];
		}
	}

	public int getNrArkusza() {
		return nrArkusza[getMiesiac()];
	}

	public int getPrevNrArkusza() {
		if (getMiesiac() > 0) {
			return nrArkusza[getMiesiac() - 1];
		} else {
			return nrArkusza[11];
		}
	}

	public String getIP() {
		try {
			String s = InetAddress.getLocalHost().getHostAddress();
			String[] iptab = s.split("\\.");
			return iptab[2];
		} catch (UnknownHostException e) {
			System.out.println(e);
			return "ERROR";
		}
	}

	public int getNumberNurse() {
		try {
			int i = Integer.parseInt(getIP());
			return (i - 100);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public String[] getPathFiles() {
		int rok = getRok();
		String prefix = "c:/Dziennik_Elektroniczny/";
		String exten = ".ods";
		String[] nameTemplate = nazwyPlikow[getNumberNurse()];
		String[] nameKorekt = new String[nameTemplate.length];

		if (getMiesiac() > 8) {
			for (int i = 0; i < nameTemplate.length; i++) {
				nameKorekt[i] = prefix + nameTemplate[i] + rok + exten;
			}
		} else {
			rok = rok - 1;
			for (int i = 0; i < nameTemplate.length; i++) {
				nameKorekt[i] = prefix + nameTemplate[i] + rok + exten;
			}
		}
		return nameKorekt;
	}
	
}
