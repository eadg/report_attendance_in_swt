package com.mooo.rokicki.dziennik;

public class Version {
	
	static private String v = "1.1";

	public static String getV() {
		return "Wersja " + v;
	}

	public static void setV(String v) {
		Version.v = v;
	}
}
