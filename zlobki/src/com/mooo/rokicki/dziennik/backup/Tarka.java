package com.mooo.rokicki.dziennik.backup;

import java.io.*;
import org.xeustechnologies.jtar.*;
import com.mooo.rokicki.dziennik.DateIP;

public class Tarka {
	
	private String tarName;
	private String tarPath;
	
	public Tarka() {
		DateIP dip = new DateIP();
		String s = dip.getNumberNurse() + "_"+ dip.getData() + ".tar";
		setTarName(s);
		setTarPath("c:/Dziennik_Elektroniczny/kopia_dziennikow_ZL" +s);
	}

	public String tar(String[] files) {
		try {
			System.out.println("Tworzenie kopii dzienników:");
			FileOutputStream dest = new FileOutputStream(tarPath);
			TarOutputStream out = new TarOutputStream(new BufferedOutputStream(dest));

			File[] filesToTar = new File[files.length];
			for(int i = 0 ; i<files.length ; i++) {
				filesToTar[i] = new File(files[i]);
				System.out.println(files[i]);
			}

			for (File f : filesToTar) {
				out.putNextEntry(new TarEntry(f, f.getName()));
				BufferedInputStream origin = new BufferedInputStream(new FileInputStream(f));
				int count;
				byte data[] = new byte[2048];
				while ((count = origin.read(data)) != -1) {
					out.write(data, 0, count);
				}
				out.flush();
				origin.close();
			}
			out.close();

		} catch (IOException ioe) {
			System.out.println(ioe);
		}
		System.out.println("Kopia dzienników utworzona");
		return "OK";
	}

	public String getTarName() {
		return tarName;
	}

	public void setTarName(String tarName) {
		this.tarName = tarName;
	}

	public String getTarPath() {
		return tarPath;
	}

	public void setTarPath(String tarPath) {
		this.tarPath = tarPath;
	}
}