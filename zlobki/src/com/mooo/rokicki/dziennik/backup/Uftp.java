package com.mooo.rokicki.dziennik.backup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

public class Uftp {

	private String server = "192.168.100.5";
	private int port = 21;
	private String user = "glassfish";
	private String pass = "szklo";

	public String upload(String lfile, String rfile) {
		FTPClient ftpClient = new FTPClient();
		File localFile = new File(lfile);
		boolean isdone = false;
		try {

			ftpClient.connect(server, port);
			ftpClient.login(user, pass);
			ftpClient.enterLocalPassiveMode();

			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			String remoteFile = "dzienniki/" + rfile;
			InputStream inputStream = new FileInputStream(localFile);

			System.out.println("Trwa wysyłanie kopii na serwer");
			isdone = ftpClient.storeFile(remoteFile, inputStream);
			inputStream.close();
			if (isdone) {
				System.out.println("Wysyłanie kopii ukończone");
			}

		} catch (IOException ex) {
			System.out.println("Error: " + ex.getMessage());
			ex.printStackTrace();
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
				localFile.delete();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return isdone ? "OK" : "Error";
	}
}